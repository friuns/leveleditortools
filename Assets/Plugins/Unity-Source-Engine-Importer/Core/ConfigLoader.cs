﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Engine.Source
{
#if UNITY_EDITOR
    using UnityEditor;

    [CustomEditor(typeof(ConfigLoader))]
    public class ConfigurationLoaderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUILayout.Box(ConfigLoader.BSPPath);
      
            if (GUILayout.Button("Load Source BSP"))
            {
                StudioMDLLoader.ModelsInRAM = null;
                BspLoader.Load(ConfigLoader.LevelName);
                
                for (var i = 0; i < LightmapSettings.lightmaps.Length; i++)
                {
                    var lightmapDatas = LightmapSettings.lightmaps;
                    LightmapData a = lightmapDatas[i];
                    Texture2D dir = a.lightmapColor;
                    var s = SceneManager.GetActiveScene().path;
                    var path = Path.GetDirectoryName(s) + "/" + Path.GetFileNameWithoutExtension(s) + "/lm" + i + ".png";
                    if (!File.Exists(path))
                    {
                        File.WriteAllBytes(path, dir.EncodeToPNG());
                        AssetDatabase.Refresh();
                    }
                    lightmapDatas[i].lightmapColor  = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
                    LightmapSettings.lightmaps = lightmapDatas;
                    

                    // AssetImporter.GetAtPath()

                    // AssetDatabase.CreateAsset("lightmap" + i + ".asset");
                }
                
                var configLoader = target as ConfigLoader;
                DestroyImmediate(configLoader.GetComponent<LightMapSave>());
                var c = configLoader.gameObject.AddComponent<LightMapSave>();
                c.Set(LightmapSettings.lightmaps.Select(a=>a.lightmapColor).ToArray());
                
                
            }
          
            GUILayout.Space(5);

            if (GUILayout.Button("Show/Hide Brushes"))
            {
                for (Int32 i = 0; i < BspLoader.BSP_Brushes.Count; i++)
                    BspLoader.BSP_Brushes[i].GetComponent<Renderer>().enabled =
                        !BspLoader.BSP_Brushes[i].GetComponent<Renderer>().enabled;
            }

            if (GUILayout.Button("Remove loaded objects in the scene"))
            {
                UnityEngine.Object.DestroyImmediate(BspLoader.BSP_WorldSpawn);
                RenderSettings.skybox = null;

                Debug.LogWarning("You need to restart the editor to free up RAM. TODO: need to fix that :D");
            }

            GUILayout.Space(10);
            //GUILayout.Box(ConfigLoader.VPKPath);
            GUILayout.Box(ConfigLoader.MDLPath);
            if (GUILayout.Button("Load Studio Model"))
                StudioMDLLoader.Load(ConfigLoader.ModelName);
            // "".IndexOf()
        }
    }
#endif

    public class ConfigLoader : MonoBehaviour
    {
        
        public static ConfigLoader instance;
        private void Awake()
        {
            instance = this;
        } 
        public void OnValidate()
        {
            instance = this;
        }
        public static String GamePath = @"E:\SteamLibrary\steamapps\common\Counter-Strike Source";
        public static readonly String[] ModFolders = { "cstrike", "hl2" };
        public static String LevelName = "$2000$"; // BSP
        // public static String VpkName = "cstrike_pak_dir"; // VPK - TODO
        // public static Boolean VpkUse = false; // Use VPK (not fully implemented)
        public static Boolean LoadMDL = false; //Load Only MDL file
        public static Boolean LoadLightmapsAsTextureShader = false;
        public static Boolean use3DSkybox = true;
        public static Boolean LoadMap = true;
        public static Boolean LoadInfoDecals = false; //This is just an example, you need to implement a complete decal system.
        public static Boolean DynamicLight = false;
        //HDR ONLY
        public static Boolean useHDRLighting = true;
        //HDR ONLY
        public static Boolean DrawArmature = true;
        public static String ModelName = "characters/hostage_04"; // MDL

        public static string BSPPath = GamePath + "/" + ModFolders[0] + "/maps/" + LevelName + ".bsp";
        public static string MDLPath = GamePath + "/" + ModFolders[0] + "/models/" + ModelName + ".mdl";
        // public static string VPKPath = GamePath + "/" + ModFolders[0] + "/" + VpkName + ".vpk"; //TODO
        // public static string SNDPath = GamePath + "/" + ModFolders[0] + "/sounds/"; //TODO
        public static string _PakPath
        {
            get
            {
#if !UNITY_EDITOR
                return Application.persistentDataPath + "/";
#else
                return Application.dataPath + "/_PakLevel/";
#endif
            }
        }

        public const float WorldScale = 0.0254f;
        public static List<LightmapData> lightmapsData; //Base LightmapData
        public static int CurrentLightmap = 0; //Lightmap Index Count

        void Start()
        {
            if (!LoadMDL && LoadMap)
            {
                BspLoader.Load(LevelName);
            }
            else if (LoadMDL && !LoadMap)
            {
                StudioMDLLoader.Load(ModelName);
            }
        }
    }
}