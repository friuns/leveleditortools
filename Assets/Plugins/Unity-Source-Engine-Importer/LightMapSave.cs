using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[ExecuteInEditMode]
public class LightMapSave:MonoBehaviour
{
    public Texture2D[] lms = new Texture2D[0];
    [ContextMenu("Awake")]
    public void Awake()
    {
        if (lms.Length > 0)
            Set(lms);
    }
    public void Set(Texture2D[] texture2Ds)
    {
        lms = texture2Ds;
        LightmapSettings.lightmaps = texture2Ds.Select(a => new LightmapData() {lightmapColor = a}).ToArray();
        LightmapSettings.lightmapsMode = LightmapsMode.CombinedDirectional; 
        var mrs = FindObjectsOfType<MeshRenderer>();
        foreach(var mr in mrs)
        if (mr.lightmapIndex != -1)
        {
            var s = mr.GetComponent<SaveLightmap>();
            if (!s) s = mr.gameObject.AddComponent<SaveLightmap>();
            s.Save();
        }
    }
}