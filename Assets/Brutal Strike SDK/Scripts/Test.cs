﻿using System.Collections;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
 
public class Test:MonoBehaviour
{
    public static AssetBundle bundle;
    public string path = @"C:\Users\friuns\Documents\BrutalStrike\maps\testmap.unity3dwindows";
    IEnumerator Start()
    {
        AssetBundle.UnloadAllAssetBundles(true);
        bundle = AssetBundle.LoadFromFile(path);
        var dd = AssetBundle.GetAllLoadedAssetBundles().ToArray();
        string scenePath = bundle.GetAllScenePaths()[0];
        yield return SceneManager.LoadSceneAsync(scenePath);
        bundle.Unload(false);

    }
    
}