using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using InternalRealtimeCSG;
using Microsoft.Win32;
using RealtimeCSG;
using RealtimeCSG.Components;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;


public partial class BuildToolsSDK : EditorWindow
{
    [MenuItem("Brutal Strike/Bake Lightmap", false)]
    public static void BakeLightmap()
    {
        foreach (var a in FindObjectsOfType<Light>())
        {
            a.shadows = LightShadows.Soft;
            a.lightmapBakeType = a.type == LightType.Directional ? LightmapBakeType.Mixed : LightmapBakeType.Baked;
        }
        Lightmapping.realtimeGI = false;
        LightmapSettings.lightmapsMode = LightmapsMode.CombinedDirectional;
        LightmapEditorSettings.lightmapper = LightmapEditorSettings.Lightmapper.ProgressiveCPU;
        LightmapEditorSettings.bakeResolution = 6;
        LightmapEditorSettings.mixedBakeMode = MixedLightingMode.Subtractive;
        foreach (var m in FindObjectsOfType<CSGModel>())
            m.ReceiveGI = ReceiveGI.Lightmaps;
        foreach (var r in FindObjectsOfType<Renderer>())
        {
            r.receiveShadows = true;
            r.shadowCastingMode = ShadowCastingMode.On;

            GameObjectUtility.SetStaticEditorFlags(r.gameObject, StaticEditorFlags.ContributeGI |
                                                                 // StaticEditorFlags.BatchingStatic |
                                                                 StaticEditorFlags.NavigationStatic |
                                                                 StaticEditorFlags.OccludeeStatic |
                                                                 StaticEditorFlags.OffMeshLinkGeneration |
                                                                 StaticEditorFlags.ReflectionProbeStatic);
        }
        CSGModelManager.BuildLightmapUvs(true);
        EditorSceneManager.MarkAllScenesDirty();
        Lightmapping.BakeAsync();
        
    }
    [MenuItem("Brutal Strike/BuildLevel All", false)]
    public static void BuildLevelAll()
    {
        var mc = FindObjectsOfType<MeshCollider>();
        
        foreach (var a in mc)
        {
            if (a.gameObject.layer == LayerMask.NameToLayer("Default"))
                a.gameObject.layer = LayerMask.NameToLayer("level");
        }
        BuildLevel(BuildTarget.StandaloneWindows);
        BuildLevel(BuildTarget.Android);
        var proc = new ProcessStartInfo();
        proc.FileName = Path.GetFullPath(Dirs.bundlesDir);
        proc.Verb = "open";
        proc.WindowStyle = ProcessWindowStyle.Hidden;
        Process.Start(proc );
        
    }
    
    // [MenuItem("Brutal Strike/BuildLevel PC", false)]
    public static void BuildLevel()
    {
        BuildLevel(BuildTarget.StandaloneWindows);
    }
    
    // [MenuItem("Brutal Strike/BuildLevel Android", false)]
    public static void BuildLevel2()
    {
        BuildLevel(BuildTarget.Android);
    }
    public static void BuildLevel(BuildTarget target)
    {
        var d = SceneManager.GetActiveScene();
        EditorSceneManager.SaveScene(d);
        
        var assetBundleName = d.name + (target == BuildTarget.StandaloneWindows? ".unity3dwindows" :".unity3dandroid");
        var build = new AssetBundleBuild() {assetNames = new[] {d.path}, assetBundleName = assetBundleName};
        Directory.CreateDirectory(Dirs.bundlesDir);
        // File.Delete("AssetBundles/"+assetBundleName);
        // File.Delete("AssetBundles/"+assetBundleName+".manifest");
        AssetBundleManifest man = BuildPipeline.BuildAssetBundles(Dirs.bundlesDir, new[] {build}, BuildAssetBundleOptions.ForceRebuildAssetBundle, target);
        Debug.Log(man == null ? "build failed" : "build success2 " + man.name);
#if sdk
        PlayerPrefs.SetString("assetPath", Path.GetFullPath(bundlesDir +"/"+ build.assetBundleName));
        var key = Registry.CurrentUser.OpenSubKey(@"Software\Unity\UnityEditor\Phaneron\", true);
        RegistryUtilities.CopyKey(key, "Brutal Strike", Registry.CurrentUser.OpenSubKey(@"Software\Phaneron\", true));
#endif
    }
    
}
public static class Dirs
{
    public static string bundlesDir = RootDir.path + "/Maps";
}
public static class RootDir
{
    public static string path;
    
    static RootDir()
    {
        if (!Application.isMobilePlatform)
        {
            var docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            try
            {
                var info = Directory.CreateDirectory(docs + "/BrutalStrike/");
                if (info.Exists)
                    path = info.FullName;
            }
            catch
            {
            }
        }

        if (path == null)
            path = Application.persistentDataPath + "/";
        Directory.CreateDirectory(path);
    }
    public static string GetPath(string s)
    {
        var p = path + s;
        Directory.CreateDirectory(p);
        return p;
    }
}


class MyTexturePostprocessor : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        var assetPath = Path.GetFileNameWithoutExtension(this.assetPath);
        if (assetPath.StartsWith("Lightmap") && assetPath.EndsWith("light"))
        {
            TextureImporter textureImporter  = (TextureImporter)assetImporter;
            textureImporter.isReadable = true;
        }
        
    }
}



namespace NewBlood
{
}